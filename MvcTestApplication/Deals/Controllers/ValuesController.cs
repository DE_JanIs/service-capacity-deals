﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DealsRepository;

namespace Deals.Controllers
{
    public class ValuesController : ApiController
    {
        private readonly IDealsRepo _dealRepository;

        public ValuesController(IDealsRepo dealRepository)
        {
            _dealRepository = dealRepository;
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public IEnumerable<Master_Deal> Get(int id)
        {
            return _dealRepository.GetAllMasterDeals();
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}