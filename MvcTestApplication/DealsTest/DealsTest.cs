﻿using System.Collections.Generic;
using System.Linq;
using Deals.Controllers;
using DealsRepository;
using Microsoft.Practices.Unity;
using Moq;
using NUnit.Framework;

namespace DealsTest
{
    [TestFixture]
    public class DealsTest
    {
        private readonly IDealsRepo _dealsRepo;
        private readonly Mock<IDealsRepo> _mockDealsRepo;
        private IUnityContainer _unityContainer;
        private ValuesController _valuesController;

        public DealsTest()
        {
            _dealsRepo = new DealsRepo();
            _mockDealsRepo =  new Mock<IDealsRepo>();
            _mockDealsRepo.Setup(x => x.GetAllDeals(It.IsAny<string>())).Returns(new List<Master_Deal>() { new Master_Deal()
            {
                Deal_id = 1,
                company_code = "ROCCOENTER"
            }});

            _mockDealsRepo.Setup(x => x.GetAllMasterDeals()).Returns(new List<Master_Deal>() { new Master_Deal()
            {
                Deal_id = 1,
                company_code = "ROCCOENTER"
            }});

            _unityContainer = new UnityContainer();
            _unityContainer.RegisterType<IDealsRepo, DealsRepo>();
            _unityContainer.RegisterInstance(_mockDealsRepo.Object);
            _valuesController = _unityContainer.Resolve<ValuesController>();
        }

        [Test]
        [Category("Integration Test")]
        public void GetallMasterDealsTest()
        {
           var deals = _dealsRepo.GetAllMasterDeals();
           Assert.IsNotNull(deals);
           Assert.Greater(deals.Count(),0);
        }

        [Test]
        public void GetAllPhysicalDealsTest()
        {
            var deals = _dealsRepo.GetAllPhysicalDeals();
            Assert.IsNotNull(deals);
            var phydealsCount = deals.Count();
            Assert.Greater(phydealsCount, 0);
        }

        [Test]
        public void GetDealsByCompanyTest()
        {
            var deals = _dealsRepo.GetAllDeals("ROCCOENTER");
            Assert.IsNotNull(deals);
            var phydealsCount = deals.Count();
            Assert.Greater(phydealsCount, 0);
        }

        [Test]
        public void GetDealsByCompanyTest_1()
        {
            var deals = _mockDealsRepo.Object.GetAllDeals("ROCCOENTER");
            Assert.IsNotNull(deals);
            var phydealsCount = deals.Count();
            Assert.AreEqual(phydealsCount, 1);
        }

        [Test]
        public void ValuesControllerTest()
        {
          var deals =  _valuesController.Get(5);
          Assert.IsNotNull(deals);
        }
    }
}
