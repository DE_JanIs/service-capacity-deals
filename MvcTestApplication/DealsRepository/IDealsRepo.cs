using System.Collections.Generic;

namespace DealsRepository
{
    public interface IDealsRepo
    {
        IEnumerable<Master_Deal> GetAllMasterDeals();
        IEnumerable<Master_Deal> GetAllPhysicalDeals();
        IEnumerable<Master_Deal> GetAllDeals(string companyCode);
    }
}