﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DealsRepository
{
    public class DealsRepo:IDealsRepo
    {
        public SEMS_QAEntities DealContext { get; set; }

        public DealsRepo()
        {
            DealContext =  new SEMS_QAEntities();
        }
        public IEnumerable<Master_Deal> GetAllMasterDeals()
        {
            return DealContext.Master_Deal.Take(100);
        }

        public IEnumerable<Master_Deal> GetAllPhysicalDeals()
        {
            return DealContext.Master_Deal.Where(x => x.group_code == "PHYS").Take(1000);
        }

        public IEnumerable<Master_Deal> GetAllDeals(string companyCode)
        {
            return DealContext.Master_Deal.Where(x => x.company_code == companyCode);
        }
    }

    
}
